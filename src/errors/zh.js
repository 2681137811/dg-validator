module.exports =  {
    required: '{title}不能为空',
    contains: '{title}必须包含{args}',
    equals: '{title}必须等于{pargs}',
    different: '{title}必须不等于{pargs}',
    after: '{title}必须在 {pargs} 之后',
    before: '{title}必须在 {pargs} 之前',
    alpha: '{title}只能用 (a-zA-Z) 字符组成',
    alphaDash: '{title}}只能用 (a-zA-Z_) 字符组成',
    alphaNumeric: '{title}只能用 (a-zA-Z0-9) 字符组成',
    alphaNumericDash: '{title}只能用 (a-zA-Z0-9_) 字符组成',
    ascii: '{title}必须是 ASCII 编码',
    base64: '{title}必须通过 base64 编码',
    byteLength: '{title}值得 bytes 长度不满足要求',
    creditCard: '{title}必须是信用卡号码',
    currency: '金额格式错误',
    date: '{title}必须是日期',
    decimal: '{title}必须是十进制数字',
    divisibleBy: '{title}必须是一个数组且能被 {args} 整除',
    email: '邮箱号格式错误',
    fqdn: '{title}必须是个合法的域名',
    "float": '{title}必须是个小数',
    "float:decimal": '{title}小数格式有误',
    'multiple': '{title}必须是{args}的倍数',
    fullWidth: '{title}需要包含全角字符',
    halfWidth: '{title}需要包含半角字符',
    hexColor: '{title}需要为个十六进制颜色值',
    hex: '{title}需要为十六进制',
    ip: '{title}不是正确的 IP 地址',
    ip4: '{title}不是正确的 IP (version 4) 地址',
    ip6: '{title}不是正确的 IP (version 6) 地址',
    isbn: '{title}需要为国际标准书号',
    isin: '{title}需要为证券识别编码',
    iso8601: '{title}需要为 iso8601 日期格式',
    "in": '{title}的值必须在 {args} 之中',
    "notIn": '{title}的值不能再 {args} 之中',
    "int": '{title}必须是个整数',
    "int:range": '{title}数值不在指定的范围内',
    length: '{title}长度有误',
    lowercase: '{title}必须是小写字母',
    uppercase: '{title}必须是大写字母',
    mobile: '手机号格式错误',
    mongoId: '{title}需要为 MongoDB 的 ObjectID',
    multibyte: '{title}需要包含多字节字符',
    url: '{title}需要为 url',
    order: '{title}需要为数据库查询 order 语句',
    field: '{title}需要为数据库查询 field 语句',
    image: '{title}上传的文件需要为图片',
    startWith: '{title}需要以 {args} 字符开头',
    endWith: '{title}需要以 {args} 字符结尾',
    string: '{title}必须是字符串',
    array: '{title}必须是一个数组',
    'array:range': '{name}长度不在指定范围内',
    boolean: '{title}必须是布尔值',
    object: '{title}必须是一个对象',
    regexp: '{title}格式无法通过验证',
    issn: '{title}国际标准连续出版物编号',
    uuid: '{title}必须是 UUID (version 3、4、5)',
    md5: '{title}并不是 MD5 加密字符串',
    macAddress: '{title} need a macAddress',
    numeric: '{title}需要为 mac 地址',
    dataURI: '{title}需要为 dataURI 格式',
    variableWidth: '{title}需要同时包含半角和全角字符'
}